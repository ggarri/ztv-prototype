#!/usr/bin/env bash

if [ $# -lt 1 ]; then
	echo "Missing arguments"
	exit 1
fi

if [ $1 != "android" ] && [ $1 != "ios" ] && [ $1 != "browser" ]; then
	echo "Argument $1 must be [android, ios, browser]"
	exit 1
fi

ionic cordova run $1 --device --consolelog --livereload
