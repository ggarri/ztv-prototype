import { TwilioClient } from "../webrtc/clients/twilio";
import _ from 'lodash';

const VoiceClientStatus = {
  OPEN: 0,
  DIALING: 1,
  INCOMING: 2,
  CALLING: 3,
};

export class VoiceClient {
  public identity: string;
  public isReady: boolean;
  public incomingCall: any;
  public status: any; // ENUM
  private webRtcClient: TwilioClient;
  private events: object;

  constructor(identity, webRtcClient) {
    this.identity = identity;
    this.webRtcClient = webRtcClient;
    this.events = {
      onIncoming: () => {}
    };
    this.isReady = false;
    this.incomingCall = null;
    this.webRtcClient.bind('onReady', this.onReady);
    this.webRtcClient.bind('onError', this.onError);
    this.webRtcClient.bind('onConnect', this.onConnect);
    this.webRtcClient.bind('onDisconnect', this.onDisconnect);
    this.webRtcClient.bind('onIncoming', this.onIncoming);
  }


  dial = (dialedNumber) => {
    debugger;
    this.webRtcClient.call({To: dialedNumber});
  };

  onReady = () => {
    this.isReady = true;
    this.status = VoiceClientStatus.OPEN;
    console.log('VoiceProvider::onReady');
  };

  onError = (err) => {
    console.log('VoiceProvider::onError');
    this.isReady = false;
  };

  onConnect = (conn) => {
    console.log('VoiceProvider::onConnect');
    this.status = VoiceClientStatus.CALLING;
  };

  onDisconnect = (conn) => {
    console.log('VoiceProvider::onReady');
    this.status = VoiceClientStatus.OPEN;
  };

  onIncoming = (conn) => {
    console.log('VoiceProvider::onIncoming');
    this.incomingCall = conn;
    this.status = VoiceClientStatus.INCOMING;
    this.events['onIncoming']();
  };

  acceptCall = (callFrom) => {
    // @TODO Validate callFrom
    this.webRtcClient.accept(this.incomingCall)
  };

  getIncomingCallId = () => {
    if(this.incomingCall) {
      return this.incomingCall.parameters.From;
    }
    return null;
  };

  bind = (event, callback) => {
    const allowEvents = _.keys(this.events);
    if (allowEvents.indexOf(event) === -1) {
      console.error(`Event ${event} does not exists: `, allowEvents.join(', '));
    }
    this.events[event] = callback;
  };

  getIdentity = () => {
    return this.identity;
  };

  // protected acceptCall(conn) {
  //   incomingCallerId
  // }
}
