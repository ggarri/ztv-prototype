import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { VoiceProvider } from '../../providers/voice/voice';
import { VoiceClient } from '../../model/voice-client';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  myIdentity: string;
  callTo: string;
  callFrom: string;
  voiceClient: VoiceClient;

  constructor(public navCtrl: NavController,
              private voiceProvider: VoiceProvider
  ) {
  }

  ngOnInit() {
    this.voiceProvider.getWebRtcClient()
      .then((voiceClient: VoiceClient) => {
        this.myIdentity = voiceClient.getIdentity();
        this.voiceClient = voiceClient;
        this.voiceClient.bind('onIncoming', this.incomingCall);
      })
      .catch(err => {
        console.error(err);
      });
  }

  call = () => {
    this.voiceClient.dial(this.callTo);
  };

  incomingCall = () => {
    this.callFrom = this.voiceClient.getIncomingCallId();
  };

  acceptCall = () => {
    this.voiceClient.acceptCall(this.callFrom);
  };

  getIdentity = () => {
    this.voiceClient.getIdentity();
  }

  // callSound() {
  //   return this.nativeAudio.preloadSimple('uniqueId1', 'assets/mp3/incoming_call.mp3')
  //     .then(
  //     (message) => {
  //       this.nativeAudio.play('uniqueId1').then(
  //         () => {
  //           console.log("Playing test sound...")
  //         }, (err) => {
  //           console.log("Error playing sound: ", err)
  //         });
  //     }, (err) => {
  //       console.log("Err: ", err)
  //     });
  // }
  //
  // call() {
  //   const twilioClient: TwilioClient = this.twilioProvider.getTwilioClient();
  //   this.callSound();
  //   twilioClient.call({
  //     To: this.phone
  //   })
  // }
}

