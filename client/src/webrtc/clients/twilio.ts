import _ from 'lodash';

declare var Twilio: any;

export class TwilioClientCredential {
  token: string;
  identity: string;
}

export class TwilioClient {
  private events;

  constructor(token) {
    console.log('Twilio', Twilio);
    this.events = {
      onReady: () => {
        console.log('OnReady: Not implemented');
      },
      onError: () => {
        console.log('onError: Not implemented');
      },
      onConnect: () => {
        console.log('onConnect: Not implemented');
      },
      onDisconnect: () => {
        console.log('onDisconnect: Not implemented');
      },
      onIncoming: () => {
        console.log('onIncoming: Not implemented');
      }
    };

    Twilio.Device.setup(token, {
      debug: true,
      region: 'ie1',
      audioConstraints: {
        optional: [{sourceId: 'default'}]
      }
    });

    Twilio.Device.ready(() => this.events.onReady());
    Twilio.Device.error((err) => this.events.onError(err));
    Twilio.Device.connect((conn) => this.events.onConnect(conn));
    Twilio.Device.disconnect((conn) => this.events.onDisconnect(conn));
    Twilio.Device.incoming((conn) => this.events.onIncoming(conn));
  }

  bind = (event, callback) => {
    const allowEvents = _.keys(this.events);
    if(allowEvents.indexOf(event) === -1) {
      console.error(`Event ${event} does not exists: `, allowEvents.join(', '));
    }
    this.events[event] = callback;
  };

  call = (params: { To: string }) => {
    // Setup Twilio.Device
    console.log(`Calling...${params.To}`);
    Twilio.Device.connect(params);
  };

  accept = (conn) => {
    conn.accept();
  };

  // private _updateAllDevices = () => {
  //   this.updateDevices(Twilio.Device.audio.speakerDevices.get());
  //   this.updateDevices(Twilio.Device.audio.ringtoneDevices.get());
  // };

  // updateDevices = (selectedDevices) => {
  //   Twilio.Device.audio.availableOutputDevices.forEach(function (device, id) {
  //     let isActive = (selectedDevices.size === 0 && id === 'default');
  //     selectedDevices.forEach(device => {
  //       isActive = device.deviceId === id;
  //     });
  //   });
  // }

  // tuneVolume = () => {
  //   this.conn.volume((inputVolume, outputVolume) => {
  //     // debugger;
  //   });
  // }
}
