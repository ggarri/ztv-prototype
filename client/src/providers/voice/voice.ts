import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TwilioClient, TwilioClientCredential } from '../../webrtc/clients/twilio';
import { getUserMedia } from '../../webrtc/core/navigator';
import { VoiceClient } from '../../model/voice-client'
import {AndroidPermissions} from '@ionic-native/android-permissions';

declare var Global: any;

@Injectable()
export class VoiceProvider {
  private url: string;
  private httpOptions: object;

  constructor(public http: HttpClient, private androidPermissions: AndroidPermissions) {
    console.log('Hello TwilioProvider Provider',);
    this.url = Global['twilio_server'];
    this.httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
      })
    };
  }

  public requestAudioOutputPermission(): Promise<any> {
    return this.requestPermission(this.androidPermissions.PERMISSION.CAPTURE_AUDIO_OUTPUT);
  }

  public requestRecordAudio(): Promise<any> {
    return this.requestPermission(this.androidPermissions.PERMISSION.RECORD_AUDIO);
  }

  private requestPermission(permission): Promise<any> {
    return this.androidPermissions.checkPermission(permission).then(
      result => {
        debugger;
        console.log('Has permission?', result.hasPermission);
      },
      err => {
        debugger;
        console.log("Request permissions: ", permission);
        this.androidPermissions.requestPermission(permission)
      }
    ).then(() => {
      this.androidPermissions.hasPermission(permission).then((val) => {
        debugger;
      })
    });
  }

  getWebRtcClient() {
    return new Promise((resolve, reject) => {
      this.requestAudioOutputPermission()
        .then(() => {
          return this.requestRecordAudio()
        }).then(() => {
        return getUserMedia({audio: true})
          .then(() => {
            console.log('Available media sources');
            this.http.get(`${this.url}/token`, this.httpOptions)
              .subscribe((res: TwilioClientCredential) => {
                const twilioClient = new TwilioClient(res.token);
                const voiceClient = new VoiceClient(res.identity, twilioClient);
                resolve(voiceClient);
              }, err => {
                reject(err);
              });
          })
          .catch((err: any) => {
            reject(err);
          });
      })
      });
  }


}
