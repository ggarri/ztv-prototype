import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { VoiceProvider } from '../../providers/voice/voice';
import { VoiceClient } from '../../webrtc/model/voice-client';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  myIdentity: string;
  callTo: string;
  callFrom: string;
  voiceClient: VoiceClient;

  constructor(public navCtrl: NavController,
              private voiceProvider: VoiceProvider)
  {
  }

  ngOnInit() {
    this.voiceProvider.getWebRtcClient()
    .then((voiceClient: VoiceClient) => {
      this.myIdentity = voiceClient.getIdentity();
      this.voiceClient = voiceClient;
      this.voiceClient.bind('onIncoming', this.incomingCall);
    })
    .catch(err => {
      debugger;
      console.error(err);
    });
  }

  call = () => {
    this.voiceClient.dial(this.callTo);
  };

  incomingCall = () => {
    this.callFrom = this.voiceClient.getIncomingCallId();
  };

  acceptCall = () => {
    this.voiceClient.acceptCall(this.callFrom);
  };

  getIdentity = () => {
    this.voiceClient.getIdentity();
  }
}

