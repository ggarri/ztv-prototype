/**
 * User: ggarrido
 * Date: 10/03/18
 * Time: 6:12
 * Copyright 2013 (c) Base7Booking, Palma
 */

export const getUserMedia = (constraints) => {
  // navigator.getUserMedia({
  //   audio: true,
  // }, (stream) => {
  //   debugger;
  // }, (err) => {
  //   debugger;
  // });

  if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
    // This object returns a promise
    return navigator.mediaDevices.getUserMedia(constraints);
  }

  return new Promise((resolve, reject) => {
    const getUserMedia = navigator.getUserMedia;
      // || navigator.webkitGetUserMedia
      // || navigator.mozGetUserMedia
      // || navigator.msGetUserMedia;

    if (!getUserMedia) {
      reject('ERROR: Missing `getUserMedia`');
    }

    getUserMedia(constraints, resolve, reject);
  });
};

