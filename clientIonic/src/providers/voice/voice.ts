import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TwilioClient, TwilioClientCredential } from '../../webrtc/clients/twilio';
import { getUserMedia } from '../../webrtc/core/navigator';
import { VoiceClient } from '../../webrtc/model/voice-client'

declare var Global: any;

@Injectable()
export class VoiceProvider {
  private url: string;
  private httpOptions: object;

  constructor(public http: HttpClient) {
    console.log('Hello TwilioProvider Provider',);
    this.url = Global['twilio_server'];
    this.httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
      })
    };
  }

  getWebRtcClient() {
    return new Promise((resolve, reject) => {
      getUserMedia({audio: true})
        .then(() => {
          debugger;
          console.log('Available media sources');
          this.http.get(`${this.url}/token`, this.httpOptions)
            .subscribe((res: TwilioClientCredential) => {
              const twilioClient = new TwilioClient(res.token);
              const voiceClient = new VoiceClient(res.identity, twilioClient);
              resolve(voiceClient);
            }, err => {
              reject(err);
            });
        })
        .catch((err: any) => {
          debugger;
          reject(err);
        });
    })
  }
}
