const ClientCapability = require('twilio').jwt.ClientCapability;
const VoiceResponse = require('twilio').twiml.VoiceResponse;
const MessagingResponse = require('twilio').twiml.MessagingResponse;

const nameGenerator = require('../name_generator');
const config = require('../config');

exports.tokenGenerator = function tokenGenerator() {
    const identity = nameGenerator();
    const capability = new ClientCapability({
        accountSid: config.accountSid,
        authToken : config.authToken,
    });

    capability.addScope(new ClientCapability.IncomingClientScope(identity));
    capability.addScope(new ClientCapability.OutgoingClientScope({
        applicationSid: config.twimlAppSid,
        clientName    : identity,
    }));

    // Include identity and token in a JSON response
    return {
        identity,
        token: capability.toJwt(),
    };
};

exports.voiceResponse = function voiceResponse(toNumber) {
    // Create a TwiML voice response
    const twiml = new VoiceResponse();

    if (toNumber) {
        // Wrap the phone number or client name in the appropriate TwiML verb
        // if is a valid phone number
        const attr = isAValidPhoneNumber(toNumber) ? 'number' : 'client';

        const dial = twiml.dial({
            callerId: config.callerId,
        });
        dial[attr]({}, toNumber);
    } else {
        twiml.say('Thanks for calling!');
    }

    return twiml.toString();
};

exports.triggerCall = id => {
    const options = {
        voice   : 'woman',
        language: 'en-gb',
    };

    MessagingResponse.say('Hello! And welcome to the Twilio App..', options);

    // The id will help you customize the response per user. This was set in
    // Trigger call > call.js while trigerring the call

    if (id === '1') {
        MessagingResponse
            .say('Hello World!!')
            .say('Now you will hear a sound', options)
            .play('http://823b4722.ngrok.io/magic-chime-01.mp3') /** http://www.soundjay.com **/
            .say('Yaaaayyyy!!');
    } else {
        MessagingResponse
            .say('Waaaasssupppp!!')
            .say('Now you will hear a sound', options)
            .play('http://823b4722.ngrok.io/magic-chime-02.mp3') /** http://www.soundjay.com **/
            .say('Yaaaayyyy!!', options);
    }
    MessagingResponse.toString();
};

/**
 * Checks if the given value is valid as phone number
 * @param {Number|String} number
 * @return {Boolean}
 */
function isAValidPhoneNumber(number) {
    return /^[\d\+\-\(\) ]+$/.test(number);
}
