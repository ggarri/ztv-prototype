const Router = require('express').Router;

const { tokenGenerator, voiceResponse } = require('./handler');

const router = new Router();

/**
 * Generate a Capability Token for a Twilio Client user - it generates a random
 * username for the client requesting a token.
 */
router.get('/token', (req, res) => {
    res.send(tokenGenerator());
});

router.post('/voice', (req, res) => {
    res.set('Content-Type', 'text/xml');
    res.send(voiceResponse(req.body.To));
});

// router.post('/call/:id', (req, res) => {
//     callTrigger(req.params.id);
//     res.writeHead(200, {
//         'Content-Type': 'text/xml',
//     });
//     res.end();
// });

// router.post('/sendmsg', (req, res) => {
//     const msg = req.body;
//     if (!msg || !msg.to || !msg.text) {
//         res.status(401).json({
//             status : 'error',
//             message: 'invalid data',
//         });
//     } else {
//         sendMsg(msg.to, msg.text, (err, message) => {
//             if (err) {
//                 res.status(401).json({
//                     status : 'error',
//                     message: 'invalid data',
//                 });
//             } else {
//                 res.status(401).json({
//                     status : 'success',
//                     message: message.sid,
//                 });
//             }
//         });
//     }
// });


// router.post('/call/:id', function (req, res) {
//     const twilio = require('twilio');
//     const twiml = new twilio.TwimlResponse();
//
//     const options = {
//         voice   : 'woman',
//         language: 'en-gb',
//     };
//
//     twiml.say('Hello! And welcome to the Twilio App..', options);
//
//     // The id will help you customize the response per user. This was set in
//     // Trigger call > call.js while trigerring the call
//
//     if (req.params.id == '1') {
//         twiml
//             .say('Hello World!!')
//             .say('Now you will hear a sound', options)
//             .play('http://823b4722.ngrok.io/magic-chime-01.mp3') /** http://www.soundjay.com **/
//             .say('Yaaaayyyy!!');
//     } else {
//         twiml
//             .say('Waaaasssupppp!!')
//             .say('Now you will hear a sound', options)
//             .play('http://823b4722.ngrok.io/magic-chime-02.mp3') /** http://www.soundjay.com **/
//             .say('Yaaaayyyy!!', options);
//     }
//
//     res.writeHead(200, {
//         'Content-Type': 'text/xml',
//     });
//     res.end(twiml.toString());
// });

module.exports = router;
